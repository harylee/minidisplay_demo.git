# minidisplay_demo

#### 介绍
该仓中的文件是minidisplay demo的gn配置文件

#### 使用说明

1.  获取[OpenHarmony](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/get-code/%E6%BA%90%E7%A0%81%E8%8E%B7%E5%8F%96.md)完整仓代码。
2.  参考[快速入门](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/Readme-CN.md)搭建环境。
3.  cd进入vendor/huawei目录，git clone https://gitee.com/harylee/minidisplay_demo.git 。
4.  cd进入device目录，git clone https://gitee.com/harylee/st.git 。
5.  下载arm交叉编译器，git clone https://gitee.com/harylee/gcc-arm-none-eabi-10-2020-q4-major.git ，将交叉编译器环境变量bin目录配置到.bashrc文件中或者配置device/st/stm32l4r9i_disco/liteos_m/config.gni文件中board_toolchain_path宏为交叉编译器bin路径。。
6.  在OpenHarmony根目录：
```
haryslee@dev:~/OpenHarmony$ hb set
[OHOS INFO] Input code path:
OHOS Which product do you need?  minidisplay_demo@huawei
haryslee@dev:~/OpenHarmony$ hb build
```
7.  最终的镜像生成在out/stm32l4r9i_disco/minidisplay_demo/目录中，通过STM32 ST-LINK Utility软件将DISCO_L4R9I.hex下载至单板即可。
```
haryslee@dev:~/OpenHarmony$ ll out/stm32l4r9i_disco/minidisplay_demo/
total 3836
drwxr-xr-x 1 haryslee haryslee     512 Apr 13 15:38 ./
drwxr-xr-x 1 haryslee haryslee     512 Apr 13 15:38 ../
-rw-r--r-- 1 haryslee haryslee     300 Apr 13 15:38 .ninja_deps
-rw-r--r-- 1 haryslee haryslee   15740 Apr 13 15:38 .ninja_log
-rwxr-xr-x 1 haryslee haryslee  522952 Apr 13 15:38 DISCO_L4R9I.bin*
-rwxr-xr-x 1 haryslee haryslee  635092 Apr 13 15:38 DISCO_L4R9I.elf*
-rw-r--r-- 1 haryslee haryslee 1470992 Apr 13 15:38 DISCO_L4R9I.hex
-rw-r--r-- 1 haryslee haryslee 1182643 Apr 13 15:38 DISCO_L4R9I.map
-rw-r--r-- 1 haryslee haryslee     365 Apr 13 15:38 args.gn
-rw-r--r-- 1 haryslee haryslee   65773 Apr 13 15:38 build.log
-rw-r--r-- 1 haryslee haryslee    4617 Apr 13 15:38 build.ninja
-rw-r--r-- 1 haryslee haryslee    1572 Apr 13 15:38 build.ninja.d
drwxr-xr-x 1 haryslee haryslee     512 Apr 13 15:38 gen/
drwxr-xr-x 1 haryslee haryslee     512 Apr 13 15:38 libs/
drwx------ 1 haryslee haryslee     512 Apr 13 15:38 obj/
-rw-r--r-- 1 haryslee haryslee    4600 Apr 13 15:38 toolchain.ninja
```

```
注：
1、确保usb stlink连接线无问题；
2、若镜像烧录后系统无响应，请重新插拔stlink供电线。
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

